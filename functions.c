/* ========================================
 *
 * Funktiot joita käytetään Zumon toimintaan linjaseurannassa ja sumopainissa
 *
 * ========================================
*/

#include <project.h>
#include <stdio.h>
#include "FreeRTOS.h"
#include "task.h"
#include "Motor.h"
#include "Ultra.h"
#include "Nunchuk.h"
#include "Reflectance.h"
#include "Gyro.h"
#include "Accel_magnet.h"
#include "LSM303D.h"
#include "IR.h"
#include "Beep.h"
#include "mqtt_sender.h"
#include <time.h>
#include <sys/time.h>
#include "serial1.h"
#include <unistd.h>
#include <math.h>


// Paristojen varauksen seurantafunktio
void check_batteries() {
        
    int16 adcresult = 0;
    float volts = 0.0;
        
    ADC_Battery_StartConvert(); // Haetaan patterien varaustaso
    if(ADC_Battery_IsEndConversion(ADC_Battery_WAIT_FOR_RESULT)) {
        adcresult = ADC_Battery_GetResult16();      // saadaan ADC arvo (0 - 4095)
        volts = (adcresult * 1.5) * (5.0 / 4095);   // Muunnetaan ADC arvo volteiksi
    }
    if(volts < 4.0){    // Jos voltit < 4.0 sytytä led
        BatteryLed_Write(1);
    } else {    // Muuten led on pois päältä
        BatteryLed_Write(0);
    }
}


// TANKKIKÄÄNNÖS
// speed: kääntymisnopeus
// delay: kääntymisen kesto
// bool right: 1 = käännytään oikealle | 0 = käännytään vasemmalle
// slow_multip: lisää kerroin (< 1) toisen puolen moottorin hidastamiseksi
void tank_turn(uint8 speed,uint32 delay,bool right,float slow_multip) { 
    if (right){
        MotorDirLeft_Write(0);      // Asetetaan vasen moottori pyörimään eteenpäin
        MotorDirRight_Write(1);     // Asetetaan oikea moottori pyörimään taaksepäin
        PWM_WriteCompare1(speed); 
        PWM_WriteCompare2(speed * slow_multip); 
        vTaskDelay(delay);
    }
    if (!right){
        MotorDirLeft_Write(1);      // Asetetaan vasen moottori pyörimään taaksepäin
        MotorDirRight_Write(0);     // Asetetaan oikea moottori pyörimään eteenpäin
        PWM_WriteCompare1(speed * slow_multip); 
        PWM_WriteCompare2(speed); 
        vTaskDelay(delay);
    }
}  
// Tank Reverse, eli peruutusfunktio
void tank_reverse(uint8 l_speed, uint8 r_speed, uint32 delay) { 
    MotorDirLeft_Write(1);      // Asetetaan vasen moottori pyörimään taaksepäin
    MotorDirRight_Write(1);     // Asetetaan oikea moottori pyörimään taaksepäin
    
    PWM_WriteCompare1(l_speed); 
    PWM_WriteCompare2(r_speed);

    vTaskDelay(delay);
}
// Napin odotus funktio
void button_wait() {
    bool nappi = false;
    while (SW1_Read() == 1 && nappi == false) {
        vTaskDelay(100);
    }
    nappi = true;
}

// Kiihtyvyysanturia käyttävä törmäystunnistus funktio
void collisioncheck(){
    // Alustetaan muuttujat
    struct accData_ data;
    TickType_t hit_timestamp;
    LSM303D_Read_Acc(&data);
    double initangle = 0;
    double hitangle = 0;
    
    // Haetaan X ja Y arvot 10ms välein
    int x = data.accX;
    int y = data.accY;
    vTaskDelay(10);
    LSM303D_Read_Acc(&data);
    int new_x = data.accX;
    int new_y = data.accY;
    // Ja verrataan niiden arvoja toisiinsa. Jos muutos menee yli raja-arvon, niin suoritetaan laskutoimitus jolla saadaan kulma
    if ((new_x - x >= 15000 || new_x - x <= -15000) || (new_y - y >= 15000 || new_y - y <= -15000)){
        hit_timestamp = xTaskGetTickCount();
        initangle = atan2((double)new_y ,(double) -new_x);  // Lasketaan atan2 viimeisimmistä y ja -x arvoista
        if (initangle < 0 ){                        // Jos saatu arvo on alle 0, lisätään 2*Pi
            initangle += 2*M_PI; 
        }
        hitangle = initangle * (180 / M_PI);        // Muutetaan initanglen tulos asteiksi
        
        x = new_x;      // Asetetaan nykyiset arvot x muuttujaan
        y = new_y;      // Asetetaan nykyiset arvot y muuttujaan
        
        print_mqtt("Zumo062/hit", "%d %f", hit_timestamp, hitangle);    // Tulostetaan aika ja kulma
    }

}

// Kiihdytysfunktio, tasainen kiihdytys / jarrutus 0-255 nopeuteen
// accel_time:  Kokonaisaika kiihdytykselle maksimi- tai miniminopeuteen
// forward:     Suunta eteenpäin 1, taaksepäin 0
// decelerate:  Kiihdyttäessä 1 (Kun halutaan nopeudesta 0 -> 255), hidastaessa 0 (kun halutaan nopeudesta 255 -> 0)
void accelerate(int accel_time, bool forward, bool decelerate){
    
    TickType_t accel_start = xTaskGetTickCount();
    TickType_t current_time = xTaskGetTickCount();
    TickType_t accel_end = accel_start + accel_time;
    TickType_t subtracted_time = accel_end - accel_start;
    float speed = 0;    // Kerroin nopeudelle 0 - 1
    
    if (forward){   // Ajetaan eteenpäin
        while (current_time <= accel_end){      // Kiihdytetään / hidastetaan kunnes tavoiteaika on saavutettu
            current_time = xTaskGetTickCount();
            speed = ((float)accel_end - (float)current_time) / (float)subtracted_time;  // Kerroin nopeudelle = tavoiteaika - tämän hetken aika / delta aika.
            if (decelerate == 0) {  // Jos hidastetaan,
                speed = 1 - speed;  // Niin nopeuden kerroin on käänteinen
            }
            motor_forward(255* speed, 1);   // Muuten kerroin on normaali, eli kiihdytetään
        }
    }
    else {      // Ajetaan taaksepäin
        while (current_time <= accel_end){      // Kiihdytetään / hidastetaan kunnes tavoiteaika on saavutettu
            current_time = xTaskGetTickCount();
            speed = ((float)accel_end - (float)current_time) / (float)subtracted_time;  // Kerroin nopeudelle = tavoiteaika - tämän hetken aika / delta aika.
            if (decelerate == 1) {  // Jos kiihdytetään
                speed = 1 - speed;  // Nopeuden kerroin on käänteinen
            }
            tank_reverse(255 * speed, 255 * speed, 1);  // Muuten kerroin on normaali
        }
    }     
}

// Destroy funktio Sumopainiin, joka etsii muita botteja ja työntää ne ulos kentältä
// Tätä funktiota kutsutaan mainissa, kun nähdään ultraäänisensorilla kohde
void destroy() {

    // Alustetaan muuttujat
    TickType_t time_stamp;
    TickType_t target_time_stamp;
    TickType_t time_now;
    int d = Ultra_GetDistance();
    struct sensors_ dig;
    
    // Luetaan ultraääni- ja pohjasensoreiden arvoja joka kierroksella
    reflectance_digital(&dig);
    d = Ultra_GetDistance();

    // Jos nähdään viiva oikealla puolella
    if (dig.r1 == 1 || dig.r2 == 1 || dig.r3 == 1) {
        time_stamp = xTaskGetTickCount();
        time_now = xTaskGetTickCount();
        target_time_stamp = time_stamp + 600;
        
        // Tankkikäännös oikealle
        tank_turn(200,15,1,1);
        while (time_now < target_time_stamp && (dig.l3 == 0 || dig.r3 == 0)) {  // Peruutetaan, kunnes tavoiteaika on kulunut, tai tullaan viivalle
            reflectance_digital(&dig);
            time_now = xTaskGetTickCount();
            d = Ultra_GetDistance();  
            tank_reverse(255, 250, 1);
        }
        accelerate(180, 0, 1);
            
        }
    // Jos nähdään viiva vasemmalla puolella
    if (dig.l3 == 1 || dig.l2 == 1 || dig.l1 == 1) {
        time_stamp = xTaskGetTickCount();
        time_now = xTaskGetTickCount();
        target_time_stamp = time_stamp + 600;
            
        // Tankkikäännös vasemmalle
        tank_turn(200,15,0,1);
        while (time_now < target_time_stamp && (dig.l3 == 0 || dig.r3 == 0)) {  // Peruutetaan, kunnes tavoiteaika on kulunut, tai tullaan viivalle
            reflectance_digital(&dig);
            time_now = xTaskGetTickCount();
            d = Ultra_GetDistance();                
            tank_reverse(255, 250, 1);
        }
        accelerate(180, 0, 1);

    }
    else {
        if (PWM_ReadCompare1() < 50 && PWM_ReadCompare2() < 50) {   // Jos moottoreiden vauhti on hidas, niin kiihdytetään
            accelerate(120, 1, 1);
        }
        motor_forward(255,1);       // Normaalisti liikutaan eteenpäin (pusketaan muita ulos kentältä)
    }
}

/* [] END OF FILE */
