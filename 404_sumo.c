/* ========================================
 *
 *  SUMO PROJEKTI - TEAM 404
 *
 * ========================================
 */
#include <project.h>
#include <stdio.h>
#include "FreeRTOS.h"
#include "task.h"
#include "Motor.h"
#include "Ultra.h"
#include "Nunchuk.h"
#include "Reflectance.h"
#include "Gyro.h"
#include "Accel_magnet.h"
#include "LSM303D.h"
#include "IR.h"
#include "Beep.h"
#include "mqtt_sender.h"
#include <time.h>
#include <sys/time.h>
#include "serial1.h"
#include <unistd.h>
#include <math.h>
#include "functions.h"

#if 1

    void zmain(void) {
    
        // Aloitetaan pattereiden seuranta
        ADC_Battery_Start();
            
        // Alustetaan ajastimen muuttujat
        TickType_t timer_start = 0;
        TickType_t timer_end = 0;
        int time = 0;
        
        // Käynnistetään pohjan sensorit ja alustetaan arvot
        reflectance_start();
        reflectance_set_threshold(18000, 18000, 16000, 16000, 18000, 18000); // set center sensor threshold to 11000 and others to 9000
        // 
        struct sensors_ dig;
        struct accData_ data;
        
        motor_start();              // enable motor controller
        motor_forward(0,0);         // set speed to zero to stop motors
        
        // Alustetaan kiihtyvyysanturin lukija
        if(!LSM303D_Start()){
            printf("LSM303D failed to initialize!!! Program is Ending!!!\n");
            vTaskSuspend(NULL);
        }
        else {
            printf("Device Ok...\n");
        }
        
        Ultra_Start();  // Ultrasonic Start function
        IR_Start();     // IR Start function
        IR_flush(); // clear IR receive buffer

        int d = Ultra_GetDistance();
        
        button_wait();                      // Odotetaan napin painallusta
        reflectance_digital(&dig);          // Luetaan pohjasensorien arvo
        while(dig.l3 == 0 || dig.r3 == 0){  // Liikutaan viivalle
            reflectance_digital(&dig);
            motor_forward(60, 5);   
        }
        if (dig.l3 == 1 || dig.r3 == 1){    // Kun saavutaan viivalle, mennään siitä yli
            while(dig.l3 == 1 || dig.r3 == 1){
                reflectance_digital(&dig);
                motor_forward(60, 5);
            }
        }
        timer_start = xTaskGetTickCount();      // Aloitetaan ajanlasku
        
        motor_forward(0,0);                             // eka viiva, pysähdytään odottamaan IR komentoa
        print_mqtt("Zumo062/ready", "zumo");            // Printataan MQTT viesti
        IR_wait();                                      // Odotetaan IR signaalia
        timer_start = xTaskGetTickCount();              // Aloitetaan ajanlaskenta
        print_mqtt("Zumo062/start", "%d", timer_start); // Printataan MQTT viesti
        accelerate(150,1,0);        // Lähdetään kiihdyttämään
        motor_turn(255,240,150);    // ajetaan eteenpäin
        accelerate(150,1,1);        // Hidastetaan
        motor_forward(0,0);         // Pysähdytään
        
        // Aloitetaan päälooppi
        for (;;) {
            check_batteries();          // Luetaan patterien arvoja
            reflectance_digital(&dig);  // Luetaan pohjasensorien arvoja
            d = Ultra_GetDistance();    // Luetaan ultraäänisensorin arvoja
            
            // SEARCH & DESTROY!
            if (d < 40){    // DESTROY // Jos ultraäänisensori näkee jotain alle 40cm päässä
                destroy();
            }
            else {          // SEARCH //  Jos ei nähdä mitään, pyöritään kunnes kohde löytyy
                tank_turn(160, 1, 1, 1);
            }
            // Sammuta moottorit napin painalluksesta
            if (SW1_Read() == 0) {
                motor_forward(0,0);
                motor_stop();
                timer_end = xTaskGetTickCount();
                print_mqtt("Zumo062/stop", "%d", timer_end);
            }
        }
    }

#endif

/* [] END OF FILE */