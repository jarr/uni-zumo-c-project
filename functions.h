/* ========================================
 *
 *  Tämä tiedosto sisältää käytettävien funktioiden headerit
 *
 * ========================================
*/

#include <project.h>
#include <stdio.h>
#include "FreeRTOS.h"
#include "task.h"
#include "Motor.h"
#include "Ultra.h"
#include "Nunchuk.h"
#include "Reflectance.h"
#include "Gyro.h"
#include "Accel_magnet.h"
#include "LSM303D.h"
#include "IR.h"
#include "Beep.h"
#include "mqtt_sender.h"
#include <time.h>
#include <sys/time.h>
#include "serial1.h"
#include <unistd.h>
#include <math.h>


void check_batteries();
void tank_turn(uint8 speed,uint32 delay,bool right,float slow_multip);
void button_wait();
void collisioncheck();
void accelerate(int accel_time, bool forward, bool decelerate);
void destroy();

/* [] END OF FILE */
